<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('curso', 'CursosController');

Route::resource('bloco', 'BlocosController');

Route::resource('docente', 'DocentesController');

Route::resource('sala', 'SalasController');

Route::resource('disciplina', 'DisciplinasController');

Route::resource('bolsista', 'BolsistasController');

Route::resource('monitoria', 'MonitoriasController');
