<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disciplina extends Model
{
    protected $guarded = [];

    public function curso()
    {
        return $this->belongsTo(Curso::class);

    }

    public function monitoria()
    {
        return $this->belongsTo(Monitoria::class);
    }
}
