<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monitoria extends Model
{
    protected $guarded = [];

    public function horarios()
    {
        return $this->hasMany(Horario::class);

    }

    public function docente()
    {
        return $this->belongsTo(Docente::class);

    }

    public function bolsista()
    {
        return $this->belongsTo(Bolsista::class);

    }

    public function disciplina()
    {
        return $this->belongsTo(Disciplina::class);

    }

    public function sala()
    {
        return $this->belongsTo(Sala::class);

    }

    protected $hidden = ['created_at', 'updated_at'];
}
