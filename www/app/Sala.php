<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    protected $guarded = [];

    public function monitoria()
    {
        return $this->belongsTo(Monitoria::class);
    }

    public function bloco()
    {
        return $this->belongsTo(Bloco::class);
    }
}
