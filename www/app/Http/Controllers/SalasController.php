<?php

namespace App\Http\Controllers;

use App\Sala;
use Illuminate\Http\Request;

class SalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sala = Sala::with('bloco')->get();
        return response()->json($sala);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $verificarSalaDuplicada = Sala::where([
            'numero' => $request->numero,
            'bloco_id' => $request->bloco_id

        ])->count();

        if($verificarSalaDuplicada)
        {
            return response()->json(['msg' => 'Sala já existente no bloco']);
        }

        $sala = new Sala();
        $sala->numero = $request->numero;
        $sala->bloco_id = $request->bloco_id;
        $add = $sala->save();

        return response()->json(['msg' => 'Sala cadastrado com sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sala = Sala::where('id', $id)->with('bloco')->get();
        return response()->json($sala);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $verificarSalaDuplicada = Sala::where([
            'numero' => $request->numero,
            'bloco_id' => $request->bloco_id

        ])->count();

        if($verificarSalaDuplicada)
        {
            return response()->json(['msg' => 'Sala já existente no bloco']);
        }

        $sala = Sala::find($id);
        $sala->numero = $request->numero;
        $sala->bloco_id = $request->bloco_id;
        $sala->save();

        return response()->json(['msg' => 'Sala altarada com sucesso']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sala = Sala::find($id);
        $sala->delete();

        return response()->json(['msg' => 'Sala excluída']);
    }
}
