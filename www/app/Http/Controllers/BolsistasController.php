<?php

namespace App\Http\Controllers;

use App\Bolsista;
use Illuminate\Http\Request;

class BolsistasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bolsista = Bolsista::with('curso')->get();
        return response()->json($bolsista);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $verificarBolsistaDuplicado = Bolsista::where([
           'nome' => $request->nome,
           'curso_id' => $request->curso_id
        ])->count();

        if($verificarBolsistaDuplicado)
        {
            return response()->json(['msg' => 'Aluno já cadastrado como bolsista']);
        }

        $bolsista = new Bolsista();
        $bolsista->nome = $request->nome;
        $bolsista->email = $request->email;
        $bolsista->curso_id = $request->curso_id;
        $bolsista->save();

        return response()->json(['msg' => 'Bolsista cadastrado']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bolsista = Bolsista::where('id', $id)->with('curso')->get();
        return response()->json($bolsista);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $verificarBolsistaDuplicado = Bolsista::where([
            'nome' => $request->nome,
            'curso_id' => $request->curso_id
        ]);

        if($verificarBolsistaDuplicado)
        {
            return response()->json(['msg' => 'Aluno já cadastrado como bolsista']);
        }

        $bolsista = Bolsista::find($id);
        $bolsista->nome = $request->nome;
        $bolsista->email = $request->email;
        $bolsista->curso_id = $request->curso_id;
        $bolsista->save();

        return response()->json(['msg' => 'Bolsista atualizado']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bolsista = Bolsista::find($id);
        $bolsista->delete();
        return response()->json(['msg' => 'Bolsista removido com sucesso!']);
    }
}
