<?php

namespace App\Http\Controllers;

use App\Monitoria;
use Illuminate\Http\Request;

class MonitoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $monitoria = Monitoria::with('disciplina.curso')->with('docente')->with('bolsista')
                                ->with('sala.bloco')->with('horarios')->get();
        return response()->json($monitoria);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $monitoria = new Monitoria();
        $monitoria->disciplina_id = $request->disciplina_id;
        $monitoria->docente_id = $request->docente_id;
        $monitoria->bolsista_id = $request->bolsista_id;
        $monitoria->sala_id = $request->sala_id;

        $monitoria->save();

        return response()->json(['msg' => 'Monitoria cadastrada']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $monitoria = Monitoria::where('id', $id)->with('disciplina.curso')->with('docente')->with('bolsista')
        ->with('sala.bloco')->with('horario')->get();
        return response()->json($monitoria);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $monitoria = Monitoria::find($id);
        $monitoria->disciplina_id = $request->disciplina_id;
        $monitoria->docente_id = $request->docente_id;
        $monitoria->bolsista_id = $request->bolsista_id;
        $monitoria->sala_id = $request->sala_id;

        $monitoria->save();

        return response()->json(['msg' => 'Monitoria atualizada']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $monitoria = Monitoria::find($id);
        $monitoria->delete();
        return response()->json(['msg' => 'Monitoria removida com sucesso!']);
    }
}
