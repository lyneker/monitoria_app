<?php

namespace App\Http\Controllers;

use App\Disciplina;
use Illuminate\Http\Request;

class DisciplinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disciplina = Disciplina::with('curso')->get();
        return response()->json($disciplina);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $verificarDisciplinaDuplicada = Disciplina::where([
            'nome' => $request->nome,
            'curso_id' => $request->curso_id
        ])->count();

        if($verificarDisciplinaDuplicada)
        {
            return response()->json(['msg' => 'Disciplina cadastrada anteriormente']);
        }

        $disciplina = new Disciplina();
        $disciplina->nome = $request->nome;
        $disciplina->curso_id = $request->curso_id;
        $disciplina->save();

        return response()->json(['msg' => 'Disciplina Cadastrada']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $disciplina = Disciplina::where('id', $id)->with('curso')->get();
        return response()->json($disciplina);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $verificarDisciplinaDuplicada = Disciplina::where([
            'nome' => $request->nome,
            'curso_id' => $request->curso_id
        ])->count();

        if($verificarDisciplinaDuplicada)
        {
            return response()->json(['msg' => 'Disciplina cadastrada anteriormente']);
        }
        $disciplina = Disciplina::find($id);
        $disciplina->nome = $request->nome;
        $disciplina->curso_id = $request->curso_id;
        $disciplina->save();

        return response()->json(['msg' => 'Disciplina atualizada']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disciplina = Disciplina::find($id);
        $disciplina->delete();
        return response()->json(['msg' => 'Disciplina removido com sucesso!']);
    }
}
