<?php

namespace App\Http\Controllers;

use App\Bloco;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BlocosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bloco = Bloco::with('salas')->get();
        return response()->json($bloco);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validarDado = Validator::make($request->input(), [
            'numero'=> 'required|min:2|max:2'
        ]);
        if($validarDado->fails()){
            return response()->json($validarDado->errors());
        }
        $bloco = new Bloco();
        $bloco->numero = $request->numero;
        $add = $bloco->save();

        return response()->json(['msg' => 'Bloco cadastrado com sucesso']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bloco = Bloco::where('id', $id)->get();
        return response()->json($bloco);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bloco = Bloco::find($id);
        $bloco->numero = $request->numero;
        $add = $bloco->save();

        return response()->json(['msg' => 'Bloco alterado com sucesso']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bloco = Bloco::find($id);
        $bloco->delete();

        return response()->json(['msg' => 'Bloco removido com sucesso!']);
    }
}
