<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    protected $guarded = [];

    public function monitoria()
    {
        return $this->belongsTo(Disciplina::class);

    }
}
