<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $guarded = [];

    public function monitoria()
    {
        return $this->belongsTo(Monitoria::class);

    }
}
