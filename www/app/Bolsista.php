<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bolsista extends Model
{
    protected $guarded = [];

    public function monitoria()
    {
        return $this->belongsTo(Monitoria::class);
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class);
    }
}
